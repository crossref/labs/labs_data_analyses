import argparse
import utils.data_format_keys as dfk

from utils.utils import read_json, save_json


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='calculate the stats')
    parser.add_argument('-m', '--matched', type=str, required=True)
    parser.add_argument('-o', '--output', type=str)

    args = parser.parse_args()

    references = read_json(args.matched)
    n = len(references)
    print('all references: {}'.format(n))

    matched = [r for r in references if dfk.CR_ITEM_DOI in r['reference']]
    print('Currently matched references: {} ({}%)'
          .format(len(matched), 100*len(matched)/n))

    matched_crossref = [r for r in references
                        if dfk.CR_ITEM_DOI in r['reference']
                        and r['reference']['doi-asserted-by'] == 'crossref']
    print('Currently matched by Crossref: {} ({}%)'
          .format(len(matched_crossref), 100*len(matched_crossref)/n))

    matched_stq = [r for r in references
                   if dfk.CR_ITEM_DOI not in r['reference']
                   and r['DOI_from_STQ'] is not None]
    print('Additional matched references by Simple Text Query: {} ({}%)'
          .format(len(matched_stq), 100*len(matched_stq)/n))

    matched_stq_str = [r for r in references
                       if dfk.CR_ITEM_DOI not in r['reference']
                       and r['DOI_from_STQ'] is not None
                       and r['DOI_from_OpenURL'] is None
                       and len(r['reference']) > 2]
    print('Additional matched references by Simple Text Query ' +
          '(mixed with structure): {} ({}%)'
          .format(len(matched_stq_str), 100*len(matched_stq_str)/n))

    matched_ou = [r for r in references
                  if dfk.CR_ITEM_DOI not in r['reference']
                  and r['DOI_from_OpenURL'] is not None]
    print('Additional matched references by OpenURL: {} ({}%)'
          .format(len(matched_ou), 100*len(matched_ou)/n))

    matched_new = [r for r in references
                   if dfk.CR_ITEM_DOI not in r['reference']
                   and (r['DOI_from_OpenURL'] is not None
                        or r['DOI_from_STQ'] is not None)]
    print('Additional matched references in total: {} ({}%)'
          .format(len(matched_new), 100*len(matched_new)/n))

    if args.output is not None:
        save_json(matched_new, args.output)
