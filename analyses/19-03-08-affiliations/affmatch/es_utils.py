import json
import requests

from affmatch.config import ES_URL


def get_query_phrase(phrase, fields=['name.norm', 'aliases.norm',
                                     'labels.label.norm']):
    return {'query':
            {'dis_max':
             {'queries': [{'match_phrase': {field: phrase}}
                          for field in fields]}}}


def get_query_common(query, fields=['name.norm', 'aliases.norm',
                                    'labels.label.norm']):
    return {'query':
            {'dis_max':
             {'queries': [{'common':
                           {field: {'query': query,
                                    'cutoff_frequency': 0.001}}}
                          for field in fields]}}}


def get_query_acronym(acronym):
    return {'query':
            {'match':
             {'acronyms': acronym}}}


def get_query_fuzzy(query, fields=['name.norm', 'aliases.norm',
                                   'labels.label.norm']):
    return {'query':
            {'dis_max':
             {'queries': [{'match': {field: {'query': query,
                                             'fuzziness': 'AUTO'}}}
                          for field in fields]}}}


def es_search(query):
    r = requests.post('{}/_search'.format(ES_URL),
                      data=json.dumps(query),
                      headers={'Content-type': 'application/json'})
    response = r.json()
    return [d['_source'] for d in response.get('hits').get('hits', [])]
