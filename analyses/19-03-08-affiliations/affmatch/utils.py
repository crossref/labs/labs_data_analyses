import re
import unidecode


def normalize(s):
    s = re.sub('\s+', ' ', unidecode.unidecode(s).strip().lower())
    s = re.sub('(?<![a-z])univ$', 'university',
               re.sub('(?<![a-z])univ[\. ]', 'university ',
                      re.sub('(?<![a-z])u\.(?! ?[a-z]\.)', 'university ', s)))
    s = re.sub('(?<![a-z])lab$', 'laboratory',
               re.sub('(?<![a-z])lab[^a-z]', 'laboratory ', s))
    s = re.sub('(?<![a-z])inst$', 'institute',
               re.sub('(?<![a-z])inst[^a-z]', 'institute ', s))
    s = re.sub('(?<![a-z])tech$', 'technology',
               re.sub('(?<![a-z])tech[^a-z]', 'technology ', s))
    s = re.sub('(?<![a-z])u\. ?s\.', 'united states', s)
    s = re.sub('&', ' and ', re.sub('&amp;', ' and ', s))
    s = re.sub('^the ', '', s)
    s = re.sub('\s+', ' ', s.strip().lower())
    return s
