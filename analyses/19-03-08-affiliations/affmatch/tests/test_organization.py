from affmatch.organization import MatchedOrganization


class TestMatchedOrganization:

    def test_init(self):
        empty = MatchedOrganization()
        assert empty.text is None
        assert empty.score == 0
        assert empty.matching_type is None
        assert empty.organization is None

        match = MatchedOrganization(text='aff', score=60,
                                    matching_type='query', organization='obj')
        assert match.text == 'aff'
        assert match.score == 60
        assert match.matching_type == 'query'
        assert match.organization == 'obj'
