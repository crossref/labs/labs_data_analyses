from affmatch.es_utils import get_query_phrase, get_query_common, \
    get_query_acronym, get_query_fuzzy


def get_all_keys(obj):
    keys = []
    if type(obj) is dict:
        keys.extend(obj.keys())
        for v in obj.values():
            keys.extend(get_all_keys(v))
    elif type(obj) is list:
        for e in obj:
            keys.extend(get_all_keys(e))
    return keys


def get_all_values(obj):
    values = []
    if type(obj) is dict:
        for v in obj.values():
            if type(v) is str:
                values.append(v)
            else:
                values.extend(get_all_values(v))
    elif type(obj) is list:
        for e in obj:
            values.extend(get_all_values(e))
    return values


def test_get_query_phrase():
    query = get_query_phrase('affiliation')
    keys = get_all_keys(query)
    assert 'query' in keys
    assert 'dis_max' in keys
    assert 'match_phrase' in keys
    assert 'name.norm' in keys
    assert 'aliases.norm' in keys
    assert 'labels.label.norm' in keys
    assert 'affiliation' in get_all_values(query)

    query = get_query_phrase('affiliation', fields=['f1', 'f2'])
    keys = get_all_keys(query)
    assert 'query' in keys
    assert 'dis_max' in keys
    assert 'match_phrase' in keys
    assert 'f1' in keys
    assert 'f2' in keys
    assert 'affiliation' in get_all_values(query)


def test_get_query_common():
    query = get_query_common('affiliation')
    keys = get_all_keys(query)
    assert 'query' in keys
    assert 'dis_max' in keys
    assert 'common' in keys
    assert 'name.norm' in keys
    assert 'aliases.norm' in keys
    assert 'labels.label.norm' in keys
    assert 'affiliation' in get_all_values(query)

    query = get_query_common('affiliation', fields=['f1', 'f2'])
    keys = get_all_keys(query)
    assert 'query' in keys
    assert 'dis_max' in keys
    assert 'common' in keys
    assert 'f1' in keys
    assert 'f2' in keys
    assert 'affiliation' in get_all_values(query)


def test_get_query_acronym():
    query = get_query_acronym('ACRONYM')
    keys = get_all_keys(query)
    assert 'query' in keys
    assert 'match' in keys
    assert 'acronyms' in keys
    assert 'ACRONYM' in get_all_values(query)


def test_get_query_fuzzy():
    query = get_query_fuzzy('affiliation')
    keys = get_all_keys(query)
    assert 'query' in keys
    assert 'dis_max' in keys
    assert 'match' in keys
    assert 'name.norm' in keys
    assert 'aliases.norm' in keys
    assert 'labels.label.norm' in keys
    assert 'fuzziness' in keys
    values = get_all_values(query)
    assert 'affiliation' in values
    assert 'AUTO' in values

    query = get_query_fuzzy('affiliation', fields=['f1', 'f2'])
    keys = get_all_keys(query)
    assert 'query' in keys
    assert 'dis_max' in keys
    assert 'match' in keys
    assert 'f1' in keys
    assert 'f2' in keys
    assert 'fuzziness' in keys
    values = get_all_values(query)
    assert 'affiliation' in values
    assert 'AUTO' in values
