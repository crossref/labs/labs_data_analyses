from affmatch.matching import MatchingNode, MatchingGraph
from affmatch.organization import MatchedOrganization


class TestMatchingNode:

    def test_init(self):
        empty = MatchingNode('text')
        assert empty.text == 'text'
        assert empty.children == []
        assert empty.matched is None

        node = MatchingNode('text', ['ch1', 'ch2'])
        node.matched = 'obj'
        assert node.text == 'text'
        assert node.children == ['ch1', 'ch2']
        assert node.matched == 'obj'

    def test_get_children_max_score(self):
        empty = MatchingNode('text')
        assert empty.get_children_max_score() == 0

        l1 = MatchingNode('l1')
        l1.matched = MatchedOrganization(text='s1', score=60,
                                         matching_type='q',
                                         organization='obj')
        l2 = MatchingNode('l2')
        l2.matched = MatchedOrganization(text='s2', score=99,
                                         matching_type='q',
                                         organization='obj')
        l3 = MatchingNode('l3')
        l3.matched = MatchedOrganization(text='s3', score=42,
                                         matching_type='q',
                                         organization='obj')

        node1 = MatchingNode('text', [l1])
        node1.matched = MatchedOrganization(text='s2', score=99,
                                            matching_type='q',
                                            organization='obj')
        assert node1.get_children_max_score() == 60

        node2 = MatchingNode('text', [l1, l2])
        node2.matched = MatchedOrganization(text='s3', score=42,
                                            matching_type='q',
                                            organization='obj')
        assert node2.get_children_max_score() == 99

        node3 = MatchingNode('text', [node2, l3])
        assert node3.get_children_max_score() == 99

    def test_remove_descendants_links(self):
        l1 = MatchingNode('l1')
        l1.matched = 'm1'
        l1.remove_descendants_links()
        assert l1.matched == 'm1'

        l1 = MatchingNode('l1')
        l1.matched = 'm1'
        l2 = MatchingNode('l2')
        l2.matched = 'm2'
        node1 = MatchingNode('text', [l1, l2])
        node1.matched = 'm12'
        node1.remove_descendants_links()
        assert l1.matched is None
        assert l2.matched is None
        assert node1.matched == 'm12'

        l1 = MatchingNode('l1')
        l1.matched = 'm1'
        l2 = MatchingNode('l2')
        l2.matched = 'm2'
        l3 = MatchingNode('l3')
        l3.matched = 'm3'
        node1 = MatchingNode('text', [l1, l2])
        node1.matched = 'm12'
        node2 = MatchingNode('text', [node1, l3])
        node2.matched = 'm123'
        node2.remove_descendants_links()
        assert l1.matched is None
        assert l2.matched is None
        assert l3.matched is None
        assert node1.matched is None
        assert node2.matched == 'm123'

    def test_prune_links(self):
        l1 = MatchingNode('l1')
        l1.matched = MatchedOrganization(text='s1', score=60,
                                         matching_type='q',
                                         organization='obj')
        l1.prune_links()
        assert l1.matched is not None

        l1 = MatchingNode('l1')
        l1.matched = MatchedOrganization(text='s1', score=60,
                                         matching_type='q',
                                         organization='obj')
        l2 = MatchingNode('l2')
        l2.matched = MatchedOrganization(text='s2', score=99,
                                         matching_type='q',
                                         organization='obj')
        l3 = MatchingNode('l3')
        l3.matched = MatchedOrganization(text='s3', score=42,
                                         matching_type='q',
                                         organization='obj')
        node1 = MatchingNode('text', [l1, l2])
        node1.matched = MatchedOrganization(text='s2', score=99,
                                            matching_type='q',
                                            organization='obj')
        node2 = MatchingNode('text', [node1, l3])
        node2.matched = MatchedOrganization(text='s3', score=52,
                                            matching_type='q',
                                            organization='obj')
        node2.prune_links()
        assert l1.matched is not None
        assert l2.matched is not None
        assert l3.matched is not None
        assert node1.matched is not None
        assert node2.matched is None

        node3 = MatchingNode('text', [node1, l3])
        node3.matched = MatchedOrganization(text='s3', score=100,
                                            matching_type='q',
                                            organization='obj')
        node3.prune_links()
        assert l1.matched is None
        assert l2.matched is None
        assert l3.matched is None
        assert node1.matched is None
        assert node3.matched is not None

    def test_get_matching_types(self):
        empty = MatchingNode('text')
        assert len(empty.get_matching_types()) == 4

        node = MatchingNode('text', ['ch1', 'ch2'])
        assert len(node.get_matching_types()) == 3


class TestMatchingGraph:

    def test_init(self):
        graph = MatchingGraph('University of Excellence')
        assert len(graph.nodes) == 1
        assert graph.nodes[0].text == 'University of Excellence'

        graph = \
            MatchingGraph('University of Excellence and Creativity Institute')
        assert len(graph.nodes) == 3
        assert graph.nodes[0].text == 'University of Excellence'
        assert graph.nodes[1].text == 'Creativity Institute'
        assert graph.nodes[2].text == \
            'University of Excellence and Creativity Institute'

        graph = \
            MatchingGraph('University of Excellence & Creativity Institute')
        assert len(graph.nodes) == 3
        assert graph.nodes[0].text == 'University of Excellence'
        assert graph.nodes[1].text == 'Creativity Institute'
        assert graph.nodes[2].text == \
            'University of Excellence & Creativity Institute'

        graph = MatchingGraph(
            'University of Excellence &amp; Creativity Institute')
        assert len(graph.nodes) == 3
        assert graph.nodes[0].text == 'University of Excellence'
        assert graph.nodes[1].text == 'Creativity Institute'
        assert graph.nodes[2].text == \
            'University of Excellence & Creativity Institute'

        graph = MatchingGraph('University of Excellence, Creativity Institute')
        assert len(graph.nodes) == 3
        assert graph.nodes[0].text == 'University of Excellence'
        assert graph.nodes[1].text == 'Creativity Institute'
        assert graph.nodes[2].text == \
            'University of Excellence Creativity Institute'

        graph = MatchingGraph('School of Brilliance, University of ' +
                              'Excellence and Perseverance; 21-100 ' +
                              'Gallifrey: Outerspace')
        assert len(graph.nodes) == 11
        assert graph.nodes[0].text == 'School of Brilliance'
        assert graph.nodes[1].text == 'University of Excellence'
        assert graph.nodes[2].text == 'Perseverance'
        assert graph.nodes[3].text == \
            'University of Excellence and Perseverance'
        assert len(graph.nodes[3].children) == 2
        assert graph.nodes[3].children[0] == graph.nodes[1]
        assert graph.nodes[3].children[1] == graph.nodes[2]
        assert graph.nodes[4].text == \
            'School of Brilliance University of Excellence'
        assert len(graph.nodes[4].children) == 2
        assert graph.nodes[4].children[0] == graph.nodes[0]
        assert graph.nodes[4].children[1] == graph.nodes[1]
        assert graph.nodes[5].text == \
            'School of Brilliance University of Excellence and Perseverance'
        assert len(graph.nodes[5].children) == 2
        assert graph.nodes[5].children[0] == graph.nodes[0]
        assert graph.nodes[5].children[1] == graph.nodes[3]
        assert graph.nodes[6].text == '21-100 Gallifrey'
        assert graph.nodes[7].text == 'Perseverance 21-100 Gallifrey'
        assert len(graph.nodes[7].children) == 2
        assert graph.nodes[7].children[0] == graph.nodes[2]
        assert graph.nodes[7].children[1] == graph.nodes[6]
        assert graph.nodes[8].text == \
            'University of Excellence and Perseverance 21-100 Gallifrey'
        assert len(graph.nodes[8].children) == 2
        assert graph.nodes[8].children[0] == graph.nodes[3]
        assert graph.nodes[8].children[1] == graph.nodes[6]
        assert graph.nodes[9].text == 'Outerspace'
        assert graph.nodes[10].text == '21-100 Gallifrey Outerspace'
        assert len(graph.nodes[10].children) == 2
        assert graph.nodes[10].children[0] == graph.nodes[6]
        assert graph.nodes[10].children[1] == graph.nodes[9]

    def test_remove_low_scores(self):
        graph = MatchingGraph('University of Excellence, Creativity Institute')
        graph.nodes[0].matched = MatchedOrganization(text='s0', score=10,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[1].matched = MatchedOrganization(text='s1', score=100,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[2].matched = MatchedOrganization(text='s2', score=67,
                                                     matching_type='q',
                                                     organization='obj')
        graph.remove_low_scores(90)
        assert graph.nodes[0].matched is None
        assert graph.nodes[1].matched is not None
        assert graph.nodes[1].matched.text == 's1'
        assert graph.nodes[2].matched is None

    def test_prune_links(self):
        graph = MatchingGraph('School of Brilliance, University of ' +
                              'Excellence and Perseverance; 21-100 ' +
                              'Gallifrey: Outerspace')
        graph.nodes[0].matched = MatchedOrganization(text='s0', score=10,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[1].matched = MatchedOrganization(text='s1', score=80,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[2].matched = MatchedOrganization(text='s2', score=0,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[3].matched = MatchedOrganization(text='s3', score=100,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[4].matched = MatchedOrganization(text='s4', score=50,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[5].matched = MatchedOrganization(text='s5', score=47,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[6].matched = MatchedOrganization(text='s6', score=5,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[7].matched = MatchedOrganization(text='s7', score=15,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[8].matched = MatchedOrganization(text='s8', score=7,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[9].matched = MatchedOrganization(text='s9', score=60,
                                                     matching_type='q',
                                                     organization='obj')
        graph.nodes[10].matched = MatchedOrganization(text='s10', score=30,
                                                      matching_type='q',
                                                      organization='obj')
        graph.prune_links()

        assert graph.nodes[0].matched is None
        assert graph.nodes[1].matched is None
        assert graph.nodes[2].matched is None
        assert graph.nodes[3].matched is not None
        assert graph.nodes[3].matched.text == 's3'
        assert graph.nodes[4].matched is not None
        assert graph.nodes[4].matched.text == 's4'
        assert graph.nodes[5].matched is None
        assert graph.nodes[6].matched is None
        assert graph.nodes[7].matched is not None
        assert graph.nodes[7].matched.text == 's7'
        assert graph.nodes[8].matched is None
        assert graph.nodes[9].matched is not None
        assert graph.nodes[9].matched.text == 's9'
        assert graph.nodes[10].matched is None
