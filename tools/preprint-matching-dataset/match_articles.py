import json
import requests
import re
import urllib.parse

from rapidfuzz import fuzz


def crossref_rest_api_call(route, params):
    result = requests.get(f"https://api.crossref.org/{route}", params)
    code = result.status_code
    if code == 200:
        result = result.json()["message"]
    return code, result


def doi_id(doi_str):
    if doi_str is None:
        return None
    return f"https://doi.org/{doi_str}"


class SBMVPreprintStrategy:
    min_score = 0.87
    max_score_diff = 0.04
    max_query_len = 5000

    def match(self, input_data):
        if input_data.startswith("10."):
            _, article = crossref_rest_api_call(
                f"works/{urllib.parse.quote_plus(input_data)}", {}
            )
        else:
            article = json.loads(input_data)

        if "journal-article" != article.get("type", ""):
            print(article["DOI"], "wrong type")
            return "wrong type"
        created = article["created"]["date-parts"][0]
        if created[0] == 2023 and created[1] > 8:
            print(article["DOI"], "wrong date")
            return "wrong date"

        candidates = self.get_candidates(article)

        return self.match_candidates(article, candidates)

    def get_candidates(self, article):
        query = self.candidate_query(article)
        code, results = crossref_rest_api_call(
            "works", {"query.bibliographic": query, "filter": "type:posted-content"}
        )
        return [r for r in results["items"] if r.get("subtype", "") == "preprint"]

    def match_candidates(self, article, candidates):
        scores = [(r["DOI"], self.score(article, r)) for r in candidates]

        matches = [(d, s) for d, s in scores if s >= SBMVPreprintStrategy.min_score]
        top_score = max([0] + [s for _, s in matches])
        matches = [
            (d, s)
            for d, s in matches
            if top_score - s < SBMVPreprintStrategy.max_score_diff
        ]

        return [
            {
                "id": doi_id(d),
                "confidence": s,
                "strategies": [type(self).__name__],
            }
            for d, s in matches
        ]

    def candidate_query(self, article):
        title = " ".join(article["title"])
        title = re.sub("mml:[a-z]*", "", title)
        title = re.sub("&amp;", "&", title)
        title = re.sub("&lt;", "<", title)
        title = re.sub("&gt;", ">", title)

        authors = " ".join([a.get("family", "") for a in article.get("author", [])])

        year = str(article["issued"]["date-parts"][0][0])

        return f"{title} {authors} {year}".strip()[: SBMVPreprintStrategy.max_query_len]

    def score(self, article, preprint):
        return (
            self.year_score(article, preprint)
            + self.title_score(article, preprint)
            + self.authors_score(article, preprint)
        ) / 3

    def year_score(self, article, preprint):
        article_year = article["issued"]["date-parts"][0][0] or 0
        preprint_year = preprint["issued"]["date-parts"][0][0] or 0
        return 1.0 if -1 <= article_year - preprint_year <= 3 else 0.0

    def title_score(self, article, preprint):
        at = article["title"][0].lower().strip() if article["title"] else ""
        pt = preprint["title"][0].lower().strip() if preprint["title"] else ""
        score = fuzz.token_set_ratio(at, pt) / 100

        def differ_by_keywords(title1, title2):
            two_words_1 = " ".join(title1.split()[:2])
            two_words_2 = " ".join(title2.split()[:2])
            for keyword in ["correction", "response", "reply", "appendix"]:
                if (keyword in two_words_1 and keyword not in title2) or (
                    keyword not in title1 and keyword in two_words_2
                ):
                    return True
            return False

        if differ_by_keywords(at, pt):
            score /= 1.5
        return score

    def authors_score(self, article, preprint):
        article_authors = article.get("author", []).copy()
        preprint_authors = preprint.get("author", []).copy()
        sum_len = len(article_authors) + len(preprint_authors)

        if sum_len > 70:
            article_author_names = " ".join(
                [a.get("family", a.get("name", "")) for a in article_authors]
            )
            preprint_author_names = " ".join(
                [a.get("family", a.get("name", "")) for a in preprint_authors]
            )
            return (
                fuzz.token_sort_ratio(
                    article_author_names.lower(), preprint_author_names.lower()
                )
                / 100
            )

        score = 0.0
        while article_authors and preprint_authors:
            s, i, j = self.most_similar_pair(article_authors, preprint_authors)
            del article_authors[i]
            del preprint_authors[j]
            score += s
        return 2 * score / sum_len if sum_len else 0.5

    def most_similar_pair(self, authors1, authors2):
        best_score = 0.0
        index1 = 0
        index2 = 0
        for i1, a1 in enumerate(authors1):
            for i2, a2 in enumerate(authors2):
                score = self.score_author_similarity(a1, a2)
                if score > best_score:
                    best_score = score
                    index1 = i1
                    index2 = i2
                if best_score > 0.99:
                    return best_score, index1, index2
        return best_score, index1, index2

    def score_author_similarity(self, a1, a2):
        if "ORCID" in a1 and "ORCID" in a2:
            return 1.0 if a1["ORCID"] == a2["ORCID"] else 0.0
        best_score = 0.0
        for n1 in self.author_names(a1):
            for n2 in self.author_names(a2):
                score = fuzz.ratio(n1, n2) / 100
                if score > best_score:
                    best_score = score
        return best_score

    def author_names(self, author):
        n = author.get("name", "").lower()
        gi = author.get("given", "").lower()
        g = re.sub(r" .*", "", gi)
        f = author.get("family", "").lower()
        names = [n, f"{gi} {f}", f"{f} {gi}", f"{g} {f}", f"{f} {g}"]
        return set([name.strip() for name in names if name.strip()])


strategy = SBMVPreprintStrategy()
with open("data/articles.txt", "r") as fr, open("data/preprint-links.txt", "a") as fw:
    for i, line in enumerate(fr):
        print(i)
        doi = line.strip()
        data = strategy.match(doi)
        fw.write(doi)
        fw.write("\t")
        fw.write(json.dumps(data))
        fw.write("\n")
