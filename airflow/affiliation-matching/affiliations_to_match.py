import json
import sys

from pyspark.sql import SparkSession

CONFIG = json.loads(sys.argv[1])
INPUT = CONFIG["input"]
AFFS = CONFIG["affiliations"]
OUTPUT = CONFIG["output"]


def get_affiliation(item):
    has_ror = [i for i in item.get("id", []) if "ROR" == i.get("id-type", "")]
    if not has_ror and "name" in item:
        return item["name"]
    return None


def affiliations_to_match(work):
    affiliations = []
    for ct in ["author", "editor", "translator", "chair"]:
        for c in work.get(ct, []):
            for a in c.get("affiliation", []):
                aff = get_affiliation(a)
                if aff is not None:
                    affiliations.append(aff)

    for a in work.get("institution", []):
        aff = get_affiliation(a)
        if aff is not None:
            affiliations.append(aff)

    for it in ["investigator", "lead-investigator", "co-lead-investigator"]:
        for p in work.get("project", []):
            for c in p.get(it, []):
                for a in c.get("affiliation", []):
                    aff = get_affiliation(a)
                    if aff is not None:
                        affiliations.append(aff)

    return affiliations


spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()

data = spark.sparkContext.textFile(INPUT, minPartitions=10000)
data = data.map(lambda line: json.loads(line))
data = data.flatMap(affiliations_to_match)
data = data.map(lambda x: (x,))
data = spark.createDataFrame(data, ["affiliation"])
data = data.distinct()

if AFFS:
    existing = spark.sparkContext.textFile(AFFS, minPartitions=10000)
    existing = existing.map(lambda line: json.loads(line))
    existing = existing.map(lambda x: (x["affiliation"],))
    existing = spark.createDataFrame(existing, ["affiliation"])
    data = data.subtract(existing)

data.write.format("json").save(OUTPUT)
