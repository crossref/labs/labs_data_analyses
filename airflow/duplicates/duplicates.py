from airflow.decorators import dag, task

from datetime import date
from datetime import datetime
from datetime import timedelta

import boto3
import json
import base64
import requests


def get_secret(secret_name):
    region_name = "us-east-1"

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    if 'SecretString' in get_secret_value_response:
        return get_secret_value_response['SecretString']
    else:
        return base64.b64decode(get_secret_value_response['SecretBinary'])


API_URL = 'https://api.crossref.org'
MAILTO = ''#get_secret('sampling-framework-mailto')

CODE_BUCKET = get_secret('bucket-name-emr-code')
BOOTSTRAP_SAMPLES_SCRIPT = f's3://{CODE_BUCKET}/sampling-framework/bootstrap.sh'
SAMPLES_SCRIPT = f's3://{CODE_BUCKET}/sampling-framework/generate-samples.py'
BOOTSTRAP_DUPLICATES_SCRIPT = f's3://{CODE_BUCKET}/duplicates/bootstrap.sh'
DUPLICATES_SCRIPT = f's3://{CODE_BUCKET}/duplicates/detect-duplicates.py'
ALIASES_SCRIPT = f's3://{CODE_BUCKET}/duplicates/get-alias-member-ids.py'

TODAY = str(date.today())
OUTPUT_BUCKET = get_secret('bucket-name-samples-data')
OUTPUT_PREFIX = f'duplicates/{TODAY}'
 
DEFAULT_ARGS = {
    'owner': 'dtkaczyk',
    'depends_on_past': False,
    'email': [get_secret('sampling-framework-mailto')],
    'email_on_failure': False,
    'email_on_retry': False,
}


def start_spark_cluster(name, instance_count, bootstrap_script, scripts, script_configs):
    client = boto3.client('emr', region_name='us-east-1')
    return client.run_job_flow(
        Name=name,
        ReleaseLabel='emr-5.36.0',
        Applications=[{
            'Name': 'Spark'
        }],
        Instances={
            'MasterInstanceType': 'm5.xlarge',
            'SlaveInstanceType': 'm5.xlarge',
            'InstanceCount': instance_count,
            'KeepJobFlowAliveWhenNoSteps': False,
            'TerminationProtected': False
        },
        Steps=[{
            'Name': name,
            'ActionOnFailure': 'TERMINATE_CLUSTER',
            'HadoopJarStep': {
                'Jar': 'command-runner.jar',
                'Args': ['spark-submit', '--deploy-mode', 'cluster', s, json.dumps(c)]}
            } for s, c in zip(scripts, script_configs)],
        BootstrapActions=[{
            'Name': 'Python packages',
            'ScriptBootstrapAction': {
                'Path': bootstrap_script
            }
        }],
        LogUri=get_secret('s3-url-logs'),
        VisibleToAllUsers=True,
        ServiceRole='EMR_DefaultRole',
        JobFlowRole='EMR_EC2_DefaultRole'
    )

def wait_for_spark_cluster(cluster):
    client = boto3.client('emr', region_name='us-east-1')
    waiter = client.get_waiter('cluster_terminated')
    waiter.wait(
        ClusterId=cluster['JobFlowId'],
        WaiterConfig={'Delay': 60, 'MaxAttempts': 1200}
    )


@dag(
    default_args=DEFAULT_ARGS,
    schedule_interval='@once',
    catchup=False,
    dagrun_timeout=timedelta(hours=20),
    start_date=datetime(2023, 1, 9),
    tags=['data analysis']
)
def duplicates():

    @task()
    def generate_sample_contexts():
        query = {'filter': [{'type': 'journal-article'}]}
        contexts = [{'name': 'all-works',
                     'api-request': 'works',
                     'sample-size': 3000,
                     'query-parameters': query,
                     's3-prefix': 'all-works'}]
        offset = 0
        while True:
            members = requests.get(f'{API_URL}/members',
                                   {'rows': 1000,
                                    'mailto': MAILTO,
                                    'offset': offset}).json()['message']['items']
            if not members:
                break
            for m in members:
                if m.get('counts-type', {}).get('all', {}).get('journal-article', 0) < 3000:
                    continue
                contexts.append({'name': f'member-{m["id"]}',
                                 'api-request': f'members/{m["id"]}/works',
                                 'query-parameters': query,
                                 's3-prefix': f'members/{m["id"]}',
                                 'sample-size': 1000})
            offset += 1000

        session = boto3.Session()
        s3 = session.resource('s3')
        contexts_key = f'{OUTPUT_PREFIX}/samples-contexts.jsonl'
        obj = s3.Object(OUTPUT_BUCKET, contexts_key)
        obj.put(Body='\n'.join([json.dumps(c) for c in contexts]))
        return f's3://{OUTPUT_BUCKET}/{contexts_key}'

    @task()
    def start_spark_samples(contexts_file: str):
        config = {'contexts': contexts_file,
                  'samples-bucket': OUTPUT_BUCKET,
                  'samples-prefix': f'{OUTPUT_PREFIX}/samples',
                  'output-jsonl': 1,
                  'api-url': API_URL,
                  'mailto': MAILTO}
        cluster = start_spark_cluster('duplicates-samples',
                                      9,
                                      BOOTSTRAP_SAMPLES_SCRIPT,
                                      [SAMPLES_SCRIPT],
                                      [config])
        return cluster

    @task()
    def start_spark_duplicates(samples_generated: bool):
        config_all_works = {'input': f's3://{OUTPUT_BUCKET}/{OUTPUT_PREFIX}/samples/all-works.jsonl',
                            'output': f's3://{OUTPUT_BUCKET}/{OUTPUT_PREFIX}/duplicates/all-works',
                            'api-url': API_URL,
                            'mailto': MAILTO}
        config_members = {'input': f's3://{OUTPUT_BUCKET}/{OUTPUT_PREFIX}/samples/members/*.jsonl',
                          'output': f's3://{OUTPUT_BUCKET}/{OUTPUT_PREFIX}/duplicates/members',
                          'api-url': API_URL,
                          'mailto': MAILTO}
        cluster = start_spark_cluster('duplicates',
                                      9,
                                      BOOTSTRAP_DUPLICATES_SCRIPT,
                                      [DUPLICATES_SCRIPT, DUPLICATES_SCRIPT],
                                      [config_all_works, config_members])
        return cluster

    @task()
    def merge_output(duplicates_detected: bool):
        s3client = boto3.client('s3')
        paginator = s3client.get_paginator('list_objects_v2')
        data = []
        for page in paginator.paginate(Bucket=OUTPUT_BUCKET, Prefix=f'{OUTPUT_PREFIX}/duplicates/all-works'):
            for obj in page['Contents']:
                if obj['Key'].endswith('csv'):
                    content = s3client.get_object(Bucket=OUTPUT_BUCKET, Key=obj['Key'])['Body'].read().decode('utf-8').strip()
                    if content:
                        data.extend(content.split('\n'))

        session = boto3.Session()
        s3 = session.resource('s3')
        obj = s3.Object(OUTPUT_BUCKET, f'{OUTPUT_PREFIX}/duplicates/all-works.csv')
        obj.put(Body='\n'.join(data))

        data = []
        for page in paginator.paginate(Bucket=OUTPUT_BUCKET, Prefix=f'{OUTPUT_PREFIX}/duplicates/members'):
            for obj in page['Contents']:
                if obj['Key'].endswith('csv'):
                    data.extend(s3client.get_object(Bucket=OUTPUT_BUCKET, Key=obj['Key'])['Body'].read().decode('utf-8').strip().split('\n'))

        obj = s3.Object(OUTPUT_BUCKET, f'{OUTPUT_PREFIX}/duplicates/members-works.csv')
        obj.put(Body='\n'.join(data))

    @task()
    def start_spark_aliases():
        config = {'input': f's3://{OUTPUT_BUCKET}/{OUTPUT_PREFIX}/aliases-data.csv',
                  'output': f's3://{OUTPUT_BUCKET}/{OUTPUT_PREFIX}/aliases',
                  'api-url': 'https://doi.crossref.org/servlet/query',
                  'mailto': 'dtkaczyk@crossref.org'}
        cluster = start_spark_cluster('aliases',
                                      9,
                                      BOOTSTRAP_DUPLICATES_SCRIPT,
                                      [ALIASES_SCRIPT],
                                      [config])
        return cluster

    @task()
    def wait_for_spark(cluster: dict):
        wait_for_spark_cluster(cluster)
        return True

    contexts_file = generate_sample_contexts()
    spark_cluster_samples = start_spark_samples(contexts_file)
    samples_generated = wait_for_spark(spark_cluster_samples)
    spark_cluster_duplicates = start_spark_duplicates(samples_generated)
    duplicates_detected = wait_for_spark(spark_cluster_duplicates)
    merge_output(duplicates_detected)

    spark_cluster_aliases = start_spark_aliases()
    wait_for_spark(spark_cluster_aliases)

workflow = duplicates()

