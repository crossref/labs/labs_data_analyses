import argparse
import base64
import boto3
import json


def get_secret(secret_name):
    region_name = "us-east-1"
    session = boto3.session.Session()
    client = session.client(service_name="secretsmanager", region_name=region_name)
    response = client.get_secret_value(SecretId=secret_name)
    return response.get(
        "SecretString", base64.b64decode(response.get("SecretBinary", ""))
    )


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Calculate cited-by counts from a snapshot."
    )
    parser.add_argument("-i", "--input", type=str, required=True)
    parser.add_argument("-o", "--output", type=str, required=True)
    args = parser.parse_args()

    client = boto3.client("emr", region_name="us-east-1")

    response = client.run_job_flow(
        Name="citation-count",
        ReleaseLabel="emr-5.36.0",
        Applications=[{"Name": "Spark"}],
        Instances={
            "MasterInstanceType": "m5.xlarge",
            "SlaveInstanceType": "m5.xlarge",
            "InstanceCount": 17,
            "KeepJobFlowAliveWhenNoSteps": False,
            "TerminationProtected": False,
        },
        Steps=[
            {
                "Name": "citation-count",
                "ActionOnFailure": "TERMINATE_CLUSTER",
                "HadoopJarStep": {
                    "Jar": "command-runner.jar",
                    "Args": [
                        "spark-submit",
                        "--deploy-mode",
                        "cluster",
                        f"s3://{get_secret('bucket-name-emr-code')}/snapshot-jsonl/citation-counts.py",
                        json.dumps({"input": args.input, "output": args.output}),
                    ],
                },
            }
        ],
        LogUri=get_secret("s3-url-logs"),
        VisibleToAllUsers=True,
        ServiceRole="EMR_DefaultRole",
        JobFlowRole="EMR_EC2_DefaultRole",
    )

    print(json.dumps(response, indent=4, sort_keys=True, default=str))
