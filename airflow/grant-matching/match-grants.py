import json
import re
import requests
import sys
import time

from pyspark.sql import SparkSession
from retry import retry


CONFIG = json.loads(sys.argv[1])
FUNDERS_FILE = CONFIG['funders-file']
GRANTS_FILE = CONFIG['grants-file']
OUTPUT = CONFIG['output']
API_URL = CONFIG['api-url']
MAILTO = CONFIG['mailto']


def iterate_items(path, filters=None):
    cursor = '*'
    while True:
        time.sleep(1)
        params = {'mailto': MAILTO, 'cursor': cursor, 'rows': 1000}
        if filters:
            params['filter'] = filters
        data = requests.get(f'{API_URL}/{path}', params)
        data = data.json()['message']
        items = data['items']
        cursor = data['next-cursor']
        if not items:
            break
        for item in items:
            yield item

def funder_doi_norm(doi):
    return '10.13039/' + re.sub('.*\/', '', doi) if doi else ''

def grant_funder_dois(grant):
    return set([funder_doi_norm(fid['id']).lower()
                for project in grant['project']
                for funding in project['funding']
                for fid in funding['funder']['id']])

def are_aliases(funders, funder_dois_1, funder_dois_2):
    funder_dois_1 = [doi for doi in funder_dois_1 if doi and doi in funders]
    funder_dois_2 = [doi for doi in funder_dois_2 if doi and doi in funders]
    for doi_1 in funder_dois_1:
        for doi_2 in funder_dois_2:
            if doi_1 in funders[doi_2]['aliases'] or doi_2 in funders[doi_1]['aliases']:
                return True
    return False

def are_family(funders, funder_dois_1, funder_dois_2):
    funder_dois_1 = [doi for doi in funder_dois_1 if doi and doi in funders]
    funder_dois_2 = [doi for doi in funder_dois_2 if doi and doi in funders]
    for doi_1 in funder_dois_1:
        for doi_2 in funder_dois_2:
            if doi_1 in funders[doi_2]['descendants'] or doi_2 in funders[doi_1]['descendants']:
                return True
    return False

def name_matches(funders, funder_dois, funder_names, name='names'):
    for funder_doi in funder_dois:
        for funder_name in funder_names:
            if funder_name in funders[funder_doi][name]:
                return True
    return False

def candidate_relevant_awards(grant, candidate):
    for funder_object in candidate.get('funder', []):
        for award in funder_object.get('award', []):
            if award.lower() == grant['award'].lower():
                yield funder_object.get('DOI', '').lower(), funder_object.get('name', '').lower(), award.lower()
            if award.lower() == grant['DOI'].lower():
                yield funder_object.get('DOI', '').lower(), funder_object.get('name', '').lower(), award.lower()

def match_candidate(grant, candidate, funders):
    if 'grant' == candidate['type']:
        return 'GRANT'

    grant_doi = grant['DOI'].lower()
    grant_award = grant['award'].lower()
    grant_funderdois = grant_funder_dois(grant)

    candidate_awards_data = list(candidate_relevant_awards(grant, candidate))
    candidate_funderdois = [a[0] for a in candidate_awards_data]
    candidate_fundernames = [a[1] for a in candidate_awards_data]
    candidate_awards = [a[2] for a in candidate_awards_data]

    for award in candidate_awards:
        if award.endswith(grant_doi):
            return 'GRANT DOI'

    if [doi for doi in grant_funderdois if doi in candidate_funderdois]:
        return 'FUNDER DOI'

    if are_aliases(funders, grant_funderdois, candidate_funderdois):
        return 'FUNDER DOI ALIAS'
    
    if are_family(funders, grant_funderdois, candidate_funderdois):
        return 'FUNDER DOI FAMILY'

    candidate_fundernames_no_doi = [a[1] for a in candidate_awards_data if not a[0]]

    if name_matches(funders, grant_funderdois, candidate_fundernames_no_doi):
        return 'FUNDER NAME'

    if name_matches(funders, grant_funderdois, candidate_fundernames_no_doi, name='aliases-names'):
        return 'FUNDER NAME ALIAS'

    if name_matches(funders, grant_funderdois, candidate_fundernames_no_doi, name='family-names'):
        return 'FUNDER NAME FAMILY'

    return ''

@retry(tries=5, delay=10)
def match_grant(grant, funders):
    if 'award' not in grant:
        return []
    grant_doi = grant['DOI'].lower()
    grant_award = grant['award'].lower()
    links = []
    for candidate in iterate_items('works', filters=f'award.number:{grant_award},award.number:{grant_doi}'):
        matched = match_candidate(grant, candidate, funders)
        links.append((grant_doi, candidate['DOI'], grant_award, matched))
    return links

spark = SparkSession.builder \
            .config('spark.jars.packages', 'org.apache.hadoop:hadoop-aws:2.8.5') \
            .getOrCreate()

grants = spark.sparkContext.textFile(GRANTS_FILE, minPartitions=1000)

funders = spark.sparkContext.wholeTextFiles(FUNDERS_FILE)
funders = json.loads(funders.collect()[0][1])

matched = grants.flatMap(lambda g: match_grant(json.loads(g), funders))
matched = spark.createDataFrame(matched, ['grant-doi', 'research-output-doi', 'award', 'matched'])
matched.write.format('csv').save(OUTPUT)
