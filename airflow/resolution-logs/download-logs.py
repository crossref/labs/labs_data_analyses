import boto3
import gzip
import json
import os
import requests
import shutil
import sys

from pyspark.sql import SparkSession

from requests.auth import HTTPBasicAuth


CONFIG = json.loads(sys.argv[1])
BUCKET = CONFIG["bucket"]
LOGS_PREFIX = CONFIG["logs-prefix"]
LOGS_URL = CONFIG["logs-url"]
CREDENTIALS_FILE = CONFIG["credentials-file"]
FILES_SUMMARY = CONFIG["files"]

spark = (
    SparkSession.builder.config(
        "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
    )
    .config("spark.executor.instances", 4)
    .config("spark.executor.cores", 1)
    .getOrCreate()
)


def download_file(file_info):
    client = boto3.client("s3")
    credentials = (
        client.get_object(Bucket=BUCKET, Key=CREDENTIALS_FILE)["Body"]
        .read()
        .decode("utf-8")
        .strip()
    )
    auth = HTTPBasicAuth(*credentials.split(":"))

    path = file_info[1]
    gz_file = path[5:]
    txt_file = gz_file[:-3]
    with requests.get(f"{LOGS_URL}/{path}", auth=auth, stream=True) as r:
        with open(gz_file, "wb") as f:
            shutil.copyfileobj(r.raw, f)

    with gzip.open(gz_file, "rb") as f_in:
        with open(txt_file, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)
    os.remove(gz_file)

    expected_lines = int(file_info[2])
    lines = 0
    with open(txt_file, "r") as f:
        lines = sum(1 for _ in f)
    if lines != expected_lines:
        raise Exception(
            f"Incorrect number of lines: expected {expected_lines}, got {lines}"
        )

    client.upload_file(txt_file, BUCKET, f"{LOGS_PREFIX}/{txt_file}")
    os.remove(txt_file)

    return 1


summaries = [fs for fs in FILES_SUMMARY if fs[1].endswith(".gz")]
files = spark.sparkContext.parallelize(summaries, numSlices=len(summaries))
files = files.map(download_file)
files.collect()
