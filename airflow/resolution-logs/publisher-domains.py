import json
import sys
import tldextract

from pyspark.sql import SparkSession


CONFIG = json.loads(sys.argv[1])

SAMPLES_BUCKET = CONFIG["samples-bucket"]
FILE_PATTERN = CONFIG["file-pattern"]
NON_PUBLISHER_DOMAINS = CONFIG["non-publisher-domains"]
OUTPUT = CONFIG["output"]

spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()


def get_domains(member_data):
    domains = {}
    lines = member_data.split("\n")
    if len(lines) < 100:
        return []
    for line in lines:
        item = json.loads(line)["data-point"]
        if "resource" not in item:
            continue
        url = item["resource"]["primary"]["URL"]
        if url is None or not url:
            continue
        domain = tldextract.extract(url).registered_domain
        if domain is None or not domain:
            continue
        if domain not in domains:
            domains[domain] = 0
        domains[domain] += 1

    cutoff = 0.1 * len(lines)
    return [k for k, v in domains.items() if v > cutoff]


data = spark.sparkContext.wholeTextFiles(
    f"s3://{SAMPLES_BUCKET}/{FILE_PATTERN}", minPartitions=1000
)
data = data.flatMap(lambda r: get_domains(r[1])).distinct()

non_publisher_domains = spark.sparkContext.textFile(NON_PUBLISHER_DOMAINS)

domains = data.subtract(non_publisher_domains)
domains.saveAsTextFile(OUTPUT)
