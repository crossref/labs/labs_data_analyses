import boto3
import json
import sys

from pyspark.sql import SparkSession
from pyspark.sql.functions import col, row_number
from pyspark.sql.window import Window


CONFIG = json.loads(sys.argv[1])
DATE = CONFIG["date"]
PUBLISHER_DOMAINS = CONFIG["publisher-domains"]
INPUT = CONFIG["input"]
OUTPUT_BUCKET = CONFIG["output-bucket"]
OUTPUT_FILE = CONFIG["output-file"]
N = 100

spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()

logs = spark.read.option("mode", "DROPMALFORMED").csv(INPUT)
logs = logs.na.fill("").na.fill(0)
logs = logs.toDF("doi", "prefix", "member", "full_domain", "registered_domain")

window = Window.orderBy(col("count").desc())

doi = logs.groupBy("doi").count()
doi = doi.withColumn("row", row_number().over(window))
doi = doi.filter(col("row") <= N).drop("row")
doi = doi.rdd.map(lambda x: {"value": x[0], "count": x[1]}).collect()

full_domain = logs.groupBy("full_domain").count()
full_domain = full_domain.withColumn("row", row_number().over(window))
full_domain = full_domain.filter(col("row") <= N).drop("row")
full_domain = full_domain.rdd.map(lambda x: {"value": x[0], "count": x[1]}).collect()

registered_domain = logs.groupBy("registered_domain").count()

publisher_domains = (
    spark.read.option("mode", "DROPMALFORMED").text(PUBLISHER_DOMAINS).distinct()
)
publisher_domains = publisher_domains.toDF("domain")

p_domain = registered_domain.join(
    publisher_domains,
    registered_domain.registered_domain == publisher_domains.domain,
    "inner",
).drop("domain")
p_domain = p_domain.withColumn("row", row_number().over(window))
p_domain = p_domain.filter(col("row") <= N).drop("row")
p_domain = p_domain.rdd.map(lambda x: {"value": x[0], "count": x[1]}).collect()

np_domain = registered_domain.join(
    publisher_domains,
    registered_domain.registered_domain == publisher_domains.domain,
    "left_anti",
).drop("domain")
np_domain = np_domain.withColumn("row", row_number().over(window))
np_domain = np_domain.filter(col("row") <= N).drop("row")
np_domain = np_domain.rdd.map(lambda x: {"value": x[0], "count": x[1]}).collect()

s3client = boto3.client("s3")
try:
    data = (
        s3client.get_object(Bucket=OUTPUT_BUCKET, Key=OUTPUT_FILE)["Body"]
        .read()
        .decode("utf-8")
    )
    data = json.loads(data)
except s3client.exceptions.NoSuchKey:
    data = []

res_object = [o for o in data if o["about"]["logs-collected-date"] == DATE]
if res_object:
    res_object = res_object[0]
else:
    res_object = {"about": {"logs-collected-date": DATE}}
    data.append(res_object)
res_object["total-count"] = logs.count()
res_object["breakdowns"] = {
    "doi": doi,
    "full-domain": full_domain,
    "publisher-domain": p_domain,
    "non-publisher-domain": np_domain,
}
data.sort(key=lambda o: o["about"]["logs-collected-date"])
data = data[-3:]

session = boto3.Session()
s3 = session.resource("s3")
obj = s3.Object(OUTPUT_BUCKET, OUTPUT_FILE)
obj.put(Body=json.dumps(data))
